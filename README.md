# golink

Link a project from your working directory into your $GOPATH/src directory

## What does this do?

If you are using the `gvm` tool to manage go installations and package sets, you may be aware
that is includes the command `gvm linkthis` which creates a symlink to the current folder in
the currently active `$GOPATH/src` directory.

Unfortunately you still need to provide the package name you want to link, otherwise it will
just link to the current basename, making the command little more than an alias for

~~~
ln -s $PWD $GOPATH/src/<package>
~~~

In addition, several tools don't react well to symlinks in your `GOPATH/src` directory and
complain about duplicate package definitons, for example.

This `golink` utility will look for an import path for the project in the current
working directory, and then move it to the correct location in `GOPATH`, leaving behind a
symlink to the new location. In order of priority it searches:

1. Canonical import paths specified in the source code as a comment on a package declaration:

~~~
package example // import "bitbucket.org/mikehouston/golink/example"
~~~

2. Git remotes configured for the repository in the working directory. If only one remote
is present, it's assumed to be the canonical import path for other projects to use.

If there is more than one remote configured, the tool will prompt you to select one by providing
its name as a command line option:

~~~
golink -remote <name>
~~~

Running golink with no arguments will try to work out the import path automatically.

## License

This software is licensed under the MIT license.

