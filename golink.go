package main

import (
	"bufio"
	"flag"
	"io"
	"log"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/pkg/errors"
	git "gopkg.in/src-d/go-git.v4"
)

var permissions = flag.Int("perm", 0777, "Set the permission bits for any directories created")
var gopath = flag.String("gopath", os.Getenv("GOPATH"), "Use the provided path as the GOPATH being linked")
var remote = flag.String("remote", "", "Use a specific named Git remote as the import path")

var pkgPattern = regexp.MustCompile(strings.Replace(`package  (\w+)( // import "(.+)")?`, " ", `\s*`, -1))
var gitURLPattern = regexp.MustCompile("git@([^:]+):(.*)")

func main() {
	flag.Parse()

	// Locate the current $GOPATH
	log.Printf("GOPATH = %q", *gopath)

	// Locate the current working directory
	wd, err := os.Getwd()
	if err != nil {
		log.Fatal(errors.Wrap(err, "Locate working directory"))
	}

	// Check if there are go files in the current directory
	pkg, err := scanFiles(wd)
	if err != nil {
		log.Fatal(errors.Wrap(err, "Scan source files"))
	}

	// Check if we have a suitable git remote
	if pkg == "" {
		pkg, err = scanGit(wd)
		if err != nil {
			log.Fatal(errors.Wrap(err, "Reading git configuration"))
		}
	}

	if pkg != "" {
		log.Printf("Found package import path: %q", pkg)

		path := filepath.Join(*gopath, "src", pkg)
		err := movePackage(wd, path)
		if err != nil {
			if os.IsExist(errors.Cause(err)) {
				log.Fatalf("Target package already exists in $GOPATH: %s", path)
			}
			log.Fatal(errors.Wrap(err, "Link package into $GOPATH"))
		}
		return
	}

	log.Fatalf("Unable to locate canonical package name for %s", wd)
}

func movePackage(wd, path string) error {
	err := os.MkdirAll(filepath.Dir(path), os.FileMode(*permissions))
	if err != nil {
		return errors.Wrap(err, "Create parent dirs")
	}

	_, err = os.Stat(path)
	if err == nil {
		return os.ErrExist
	}
	err = os.Rename(wd, path)
	if err != nil {
		return errors.Wrap(err, "Move working directory")
	}
	log.Printf("Moved package to GOPATH: %s", path)

	err = os.Symlink(path, wd)
	if err != nil {
		return errors.Wrap(err, "Create symlink")
	}
	log.Printf("Created link: %s -> %s", wd, path)
	return nil
}

func scanFiles(dir string) (string, error) {
	target, err := os.Open(dir)
	if err != nil {
		return "", errors.Wrap(err, "Unable to list files in working directory")
	}
	defer target.Close()
	files, err := target.Readdir(-1)
	if err == io.EOF {
		return "", errors.Wrap(err, "No files in current directory")
	}
	if err != nil {
		return "", errors.Wrap(err, "Listing source files")
	}
	for _, f := range files {

		if f.IsDir() {
			if f.Name() == "vendor" {
				continue
			}

			pkg, err := scanFiles(filepath.Join(dir, f.Name()))
			if err != nil {
				return "", err
			}
			if pkg != "" {
				return filepath.Dir(pkg), nil
			}
		} else if strings.HasSuffix(f.Name(), ".go") {
			// Scan a go source file to check for canonical import paths
			pkg, err := readSourcePackage(filepath.Join(dir, f.Name()))
			if err != nil {
				return "", errors.Wrap(err, "Reading source file")
			}

			if pkg != "" {
				return pkg, nil
			}
		}
	}

	return "", nil
}

func readSourcePackage(path string) (string, error) {

	file, err := os.Open(path)
	if err != nil {
		return "", errors.Wrap(err, "Open file")
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		matches := pkgPattern.FindStringSubmatch(scanner.Text())
		if matches != nil {
			name, canonical := matches[1], matches[3]
			if canonical != "" {
				log.Printf("Found package %s with canonical path %q", name, canonical)
				return canonical, nil
			}

			// Found a package with no canonical import path
			return "", nil
		}
	}

	if err = scanner.Err(); err != nil {
		return "", errors.Wrap(err, "Scanner")
	}

	log.Printf("No package declaration found for %s", path)
	return "", nil
}

func scanGit(path string) (string, error) {
	repo, err := git.PlainOpen(path)
	if err != nil {
		return "", errors.Wrap(err, "Unable to open Git repository")
	}

	remotes, err := repo.Remotes()
	if err != nil {
		return "", errors.Wrap(err, "Unable to get remotes for repository")
	}

	if len(remotes) > 1 && *remote == "" {
		log.Println("Multiple Git remotes found:")
		for _, r := range remotes {
			log.Printf("  %s", r.Config().Name)
		}

		log.Println()
		log.Println("Use golink -remote <name> to link a specific remote as the import path for this package")
		return "", errors.Errorf("Please select a remote")
	}

	for _, r := range remotes {

		if *remote != "" && *remote != r.Config().Name {
			continue
		}

		repoURL := r.Config().URLs[0]
		matches := gitURLPattern.FindStringSubmatch(repoURL)
		if matches != nil {
			return matches[1] + "/" + matches[2], nil
		}

		url, err := url.Parse(repoURL)
		if err != nil {
			return "", errors.Wrap(err, "Unable to parse remote repository URL")
		}

		rpath := url.Path
		if strings.HasSuffix(rpath, ".git") {
			rpath = rpath[:len(rpath)-4]
		}
		return url.Host + rpath, nil
	}

	return "", nil
}
